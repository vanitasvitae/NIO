import java.nio.channels.SocketChannel;

/**
 * Created by vanitas on 05.07.17.
 */
public class DataEvent {
    public final Server server;
    public final SocketChannel socketChannel;
    public final byte[] data;

    public DataEvent(Server server, SocketChannel socket, byte[] dataCopy) {
        this.data = dataCopy;
        this.server = server;
        this.socketChannel = socket;
    }
}
