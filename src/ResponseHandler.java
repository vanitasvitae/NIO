public class ResponseHandler {
    private byte[] response = null;

    public synchronized boolean handleResponse(byte[] response) {
        this.response = response;
        this.notify();
        return true;
    }

    public synchronized String waitForResponse() {

        while(this.response == null) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }

        return new String(this.response);
    }
}

