import java.nio.channels.SocketChannel;
import java.util.LinkedList;

public class Task implements Runnable {
    private final LinkedList<DataEvent> queue = new LinkedList<>();

    public Task() {

    }

    public void processData(Server server, SocketChannel socket, byte[] data, int count) {
        byte[] dataCopy = new byte[count];
        System.arraycopy(data, 0, dataCopy, 0, count);
        synchronized(queue) {
            queue.add(new DataEvent(server, socket, dataCopy));
            queue.notify();
        }
    }


    public static DataEvent work(DataEvent event) {
        String s = new String(event.data);
        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            switch (c) {
                case 'o':
                case 'O':
                    chars[i] = '0';
                    break;
                case 'l':
                case 'L':
                    chars[i] = '1';
                    break;
                case 'z':
                case 'Z':
                    chars[i] = '2';
                    break;
                case 'e':
                case 'E':
                    chars[i] = '3';
                    break;
                case 'a':
                case 'A':
                    chars[i] = '4';
                    break;
                case 's':
                case 'S':
                    chars[i] = '5';
                    break;
                case 'g':
                case 'G':
                    chars[i] = '6';
                    break;
                case 't':
                case 'T':
                    chars[i] = '7';
                    break;
                case 'b':
                case 'B':
                    chars[i] = '8';
                    break;
            }
        }
        return new DataEvent(event.server, event.socketChannel, new String(chars).getBytes());
    }

    @Override
    public void run() {
        DataEvent result;

        while(true) {
            // Wait for data to become available
            synchronized(queue) {
                while(queue.isEmpty()) {
                    try {
                        queue.wait();
                    } catch (InterruptedException e) {
                    }
                }
                DataEvent dataEvent = queue.remove(0);
                result = work(dataEvent);
            }

            // Return to sender
            result.server.send(result.socketChannel, result.data);
        }
    }
}
