
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Client implements Runnable {
    private InetAddress hostAddress;
    private int port;

    private Selector selector;
    private ByteBuffer readBuffer = ByteBuffer.allocate(4096);
    private final List<ChangeRequest> pendingChanges = new LinkedList<>();
    private final Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<>();
    private Map<SocketChannel, ResponseHandler> responseHandlers = Collections.synchronizedMap(new HashMap<>());

    private Client(InetAddress hostAddress, int port) throws IOException {
        this.hostAddress = hostAddress;
        this.port = port;
        this.selector = SelectorProvider.provider().openSelector();
    }

    private void send(byte[] data, ResponseHandler handler) throws IOException {
        SocketChannel socket = this.initiateConnection();

        this.responseHandlers.put(socket, handler);

        synchronized (this.pendingData) {
            List<ByteBuffer> queue = this.pendingData.get(socket);
            if (queue == null) {
                queue = new LinkedList<>();
                pendingData.put(socket, queue);
            }

            queue.add(ByteBuffer.wrap(data));
        }

        this.selector.wakeup();
    }

    public void run() {
        while (true) {
            try {
                synchronized (pendingChanges) {
                    for (ChangeRequest change : this.pendingChanges) {
                        switch (change.type) {
                            case ChangeRequest.CHANGEOPS:
                                SelectionKey key = change.socket.keyFor(this.selector);
                                key.interestOps(change.ops);
                                break;
                            case ChangeRequest.REGISTER:
                                change.socket.register(this.selector, change.ops);
                                break;
                        }
                    }
                    this.pendingChanges.clear();
                }

                this.selector.select();

                Iterator selectedKeys = this.selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) selectedKeys.next();
                    selectedKeys.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    if (key.isConnectable()) {
                        this.finishConnection(key);
                    } else if (key.isReadable()) {
                        this.read(key);
                    } else if (key.isWritable()) {
                        this.write(key);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        this.readBuffer.clear();

        int numRead;
        try {
            numRead = socketChannel.read(this.readBuffer);
        } catch (IOException e) {
            key.cancel();
            socketChannel.close();
            return;
        }

        if (numRead == -1) {
            key.channel().close();
            key.cancel();
            return;
        }

        this.handleResponse(socketChannel, this.readBuffer.array(), numRead);
    }

    private void handleResponse(SocketChannel socketChannel, byte[] data, int numRead) throws IOException {
        byte[] rspData = new byte[numRead];
        System.arraycopy(data, 0, rspData, 0, numRead);

        ResponseHandler handler = responseHandlers.get(socketChannel);

        if (handler.handleResponse(rspData)) {
            socketChannel.close();
            socketChannel.keyFor(selector).cancel();
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        synchronized (pendingData) {
            List<ByteBuffer> queue = pendingData.get(socketChannel);

            if (queue == null) {
                return;
            }

            while (!queue.isEmpty()) {
                ByteBuffer buf = queue.get(0);
                socketChannel.write(buf);
                if (buf.remaining() > 0) {
                    break;
                }
                queue.remove(0);
            }

            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    private void finishConnection(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        try {
            socketChannel.finishConnect();
        } catch (IOException e) {
            System.out.println(e.toString());
            key.cancel();
            return;
        }

        key.interestOps(SelectionKey.OP_WRITE);
    }

    private SocketChannel initiateConnection() throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.connect(new InetSocketAddress(hostAddress, port));
        synchronized(pendingChanges) {
            pendingChanges.add(new ChangeRequest(socketChannel, ChangeRequest.REGISTER, SelectionKey.OP_CONNECT));
        }

        return socketChannel;
    }

    public static void main(String[] args) {
        for (int i=0; i < 2000; i++) {
            try {
                Client client = new Client(InetAddress.getLocalHost(), 9997);
                Thread t = new Thread(client);
                t.setDaemon(true);
                t.start();
                ResponseHandler handler = new ResponseHandler();
                for (int j=0; j<30; j++) {
                    client.send("Hallo Welt".getBytes(), handler);
                    System.out.println(handler.waitForResponse());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
