import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Server implements Runnable {

    private ServerSocketChannel channel;
    private Selector selector;

    private ByteBuffer buffer;

    private final Task worker;

    private final List<ChangeRequest> changeRequests = new LinkedList<>();
    private final Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<>();

    public Server(InetAddress host, int port, Task worker) throws IOException {
        this.worker = worker;

        //Selector
        selector = SelectorProvider.provider().openSelector();

        //Channel
        channel = ServerSocketChannel.open();
        channel.configureBlocking(false);   //Non-blocking
        channel.bind(new InetSocketAddress(host, port));
        channel.register(selector, SelectionKey.OP_ACCEPT);

        //Buffer
        buffer = ByteBuffer.allocate(4096);
    }

    @Override
    public void run() {
        System.out.println("Run");
        while (true) {
            System.out.println("While");
            try {
                synchronized (this.changeRequests) {
                    for (ChangeRequest change : this.changeRequests) {
                        switch (change.type) {
                            case ChangeRequest.CHANGEOPS:
                                SelectionKey key = change.socket.keyFor(this.selector);
                                key.interestOps(change.ops);
                        }
                    }
                    this.changeRequests.clear();
                }

                selector.select(); //Only blocking OP.

                Iterator<SelectionKey> keyIterator = selector.selectedKeys().iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey k = keyIterator.next();
                    keyIterator.remove();

                    if (k.isValid()) {
                        if (k.isAcceptable()) {
                            accept(k);
                        } else if (k.isReadable()) {
                            read(k);
                        } else if (k.isWritable()) {
                            write(k);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel nChannel = (ServerSocketChannel) key.channel();
        SocketChannel sChannel = nChannel.accept();
        sChannel.configureBlocking(false);
        sChannel.register(selector, SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        buffer.clear();

        int r;
        try {
            r = channel.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
            key.cancel();
            channel.close();
            return;
        }

        if (r != -1) {

            worker.processData(this, channel, buffer.array(), r);

        } else {
            channel.close();
            key.cancel();
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        synchronized (pendingData) {
            List<ByteBuffer> queue = pendingData.get(socketChannel);

            while (!queue.isEmpty()) {
                ByteBuffer buffer = queue.get(0);
                socketChannel.write(buffer);
                if (buffer.remaining() > 0) {
                    break;
                }
                queue.remove(0);
            }

            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    void send(SocketChannel socketChannel, byte[] data) {
        synchronized (changeRequests) {
            changeRequests.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

            synchronized (pendingData) {
                List<ByteBuffer> queue = this.pendingData.computeIfAbsent(socketChannel, k -> new ArrayList<>());
                queue.add(ByteBuffer.wrap(data));
            }
        }
        selector.wakeup();
    }

    public static void main(String[] args) {
        try {
            Task worker = new Task();
            new Thread(worker).start();
            new Thread(new Server(InetAddress.getLocalHost(), 9997, worker)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
